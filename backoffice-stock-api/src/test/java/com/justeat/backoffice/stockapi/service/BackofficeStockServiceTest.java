package com.justeat.backoffice.stockapi.service;


import com.justeat.backoffiice.stockapi.exception.StockApiException;
import com.justeat.backoffiice.stockapi.repository.StockSummaryRepository;
import com.justeat.backoffiice.stockapi.service.BackofficeStockServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BackofficeStockServiceTest {

    @Mock
    private StockSummaryRepository repository;
    @InjectMocks
    private BackofficeStockServiceImpl backofficeStockService;

    @Test
    void whenRestaurantNotFound_ShouldThrowRestaurantNotFoundException() {
        when(repository.getStockSummaries(1L)).thenReturn(Optional.empty());
        assertThrows(StockApiException.RestaurantNotFoundException.class, () -> backofficeStockService.getProductSummary(1L));
    }

}
