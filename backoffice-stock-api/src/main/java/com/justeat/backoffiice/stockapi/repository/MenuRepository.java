package com.justeat.backoffiice.stockapi.repository;

import com.justeat.backoffiice.stockapi.domain.Menu;
import com.justeat.backoffiice.stockapi.domain.projection.MenuP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MenuRepository extends JpaRepository<com.justeat.backoffiice.stockapi.domain.Menu, Long> {

    @Query(value = "SELECT  M.*, M.PRODUCT_VARIANT_OPTION_ID, R.NAME AS RESTAURANT, C.NAME AS CATEGORY, P.NAME AS PRODUCT," +
            "  V.NAME AS VARIANT, VO.NAME AS VARIANT_OPTION FROM" +
            " PRODUCT P, MENU M, PRODUCT_VARIANT_OPTION PVO, RESTAURANT R, " +
            " CATEGORY C, PRODUCT_CATEGORY PC, VARIANT V, VARIANT_OPTION VO" +
            " WHERE PVO.PRODUCT_ID = P.ID  " +
            " AND M.RESTAURANT_ID = R.ID" +
            " AND PVO.ID = M.PRODUCT_VARIANT_OPTION_ID " +
            " AND C.ID = PC.CATEGORY_ID" +
            " AND P.ID = PC.PRODUCT_ID" +
            " AND V.ID = VO.VARIANT_ID" +
            " AND PVO.PRODUCT_ID = P.ID" +
            " AND PVO.VARIANT_OPTION_ID = VO.ID" +
            " AND R.ID = :restaurantId" +
            " AND P.IS_ACTIVE = 1 AND M.IS_ACTIVE = 1 "
            , nativeQuery = true)
    Optional<List<Menu>> getRestaurantMenu(@Param("restaurantId") Long restaurantId);

    @Query(value = "SELECT  M.ID, R.NAME AS RESTAURANT, C.NAME AS CATEGORY, P.NAME AS PRODUCT," +
            "  V.NAME AS VARIANT, VO.NAME AS VARIANTOPTION FROM" +
            " PRODUCT P, MENU M, PRODUCT_VARIANT_OPTION PVO, RESTAURANT R, " +
            " CATEGORY C, PRODUCT_CATEGORY PC, VARIANT V, VARIANT_OPTION VO" +
            " WHERE PVO.PRODUCT_ID = P.ID  " +
            " AND M.RESTAURANT_ID = R.ID" +
            " AND PVO.ID = M.PRODUCT_VARIANT_OPTION_ID " +
            " AND C.ID = PC.CATEGORY_ID" +
            " AND P.ID = PC.PRODUCT_ID" +
            " AND V.ID = VO.VARIANT_ID" +
            " AND PVO.PRODUCT_ID = P.ID" +
            " AND PVO.VARIANT_OPTION_ID = VO.ID" +
            " AND R.ID = :restaurantId" +
            " AND P.IS_ACTIVE = 1 AND M.IS_ACTIVE = 1 "
            , nativeQuery = true)
    Optional<List<MenuP>> getRestaurantMenu1(@Param("restaurantId") Long restaurantId);

}
