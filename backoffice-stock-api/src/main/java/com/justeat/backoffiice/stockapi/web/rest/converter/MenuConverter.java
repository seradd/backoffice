package com.justeat.backoffiice.stockapi.web.rest.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.justeat.backoffiice.stockapi.domain.projection.MenuP;
import com.justeat.backoffiice.stockapi.dto.MenuDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MenuConverter {
    ObjectMapper INSTANCE = Mappers.getMapper( ObjectMapper.class );

    @Mapping(source = "id", target = "id")
    List<MenuDto> toDtoList(List<MenuP> menus);

}
