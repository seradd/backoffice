package com.justeat.backoffiice.stockapi.domain.projection;

public interface MenuP {
    Long getId();
    String getRestaurant();
    String getProduct();
    String getCategory();
    String getVariant();
    String getVariantOption();
}
