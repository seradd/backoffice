package com.justeat.backoffiice.stockapi.service;

import com.justeat.backoffiice.stockapi.domain.projection.StockSummaryP;
import com.justeat.backoffiice.stockapi.model.UpdateStockSummaryRequest;

import java.util.List;

public interface BackofficeStockService {

    void updateStockSummary(UpdateStockSummaryRequest request);

    List<StockSummaryP> getProductSummary(Long restaurantId);

}
