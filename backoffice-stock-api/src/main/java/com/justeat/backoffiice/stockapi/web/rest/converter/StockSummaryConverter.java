package com.justeat.backoffiice.stockapi.web.rest.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.justeat.backoffiice.stockapi.domain.projection.StockSummaryP;
import com.justeat.backoffiice.stockapi.dto.StockSummaryDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StockSummaryConverter {

    ObjectMapper INSTANCE = Mappers.getMapper( ObjectMapper.class );

    @Mapping(source = "id", target = "id")
    List<StockSummaryDto> toDtoList(List<StockSummaryP> menus);
}
