package com.justeat.backoffiice.stockapi.domain;

import com.justeat.backoffiice.stockapi.domain.enumeration.OperationTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
@Table(name = "STOCK_TRANSACTION")
public class StockTransaction extends BaseEntity{

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "VARIANT_OPTION_ID")
    private Long variantOptionId;

    @Enumerated(EnumType.STRING)
    @Column(name = "OPERATION_TYPE")
    private OperationTypeEnum operationType;

}
