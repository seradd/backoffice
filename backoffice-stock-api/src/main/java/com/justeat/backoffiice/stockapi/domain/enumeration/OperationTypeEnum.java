package com.justeat.backoffiice.stockapi.domain.enumeration;

public enum OperationTypeEnum {
    IN, OUT
}
