package com.justeat.backoffiice.stockapi.repository;

import com.justeat.backoffiice.stockapi.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {


}
