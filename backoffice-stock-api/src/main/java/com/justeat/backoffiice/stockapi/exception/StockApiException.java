package com.justeat.backoffiice.stockapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class StockApiException {

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class RestaurantNotFoundException extends RuntimeException{
        public RestaurantNotFoundException(String message) {
            super("Restaurant Not Found: " +message);
        }
    }
}
