package com.justeat.backoffiice.stockapi.service;

import com.justeat.backoffiice.stockapi.domain.StockSummary;
import com.justeat.backoffiice.stockapi.domain.projection.StockSummaryP;
import com.justeat.backoffiice.stockapi.exception.StockApiException;
import com.justeat.backoffiice.stockapi.model.UpdateStockSummaryRequest;
import com.justeat.backoffiice.stockapi.repository.StockSummaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class BackofficeStockServiceImpl implements BackofficeStockService {

    private final StockSummaryRepository stockSummaryRepository;

    /**
     * updates the stock status of a restaurant's product
     *
     * @param request includes menuId, restaurantId and new quantity values
     */
    @Override
    public void updateStockSummary(UpdateStockSummaryRequest request) {
        StockSummary stockSummary = new StockSummary();
        stockSummary.setTotalQuantity(request.getTotalQuantity());
        stockSummary.setMenuId(request.getMenuId());
        stockSummaryRepository.save(stockSummary);
    }

    /**
     * Lists the stock summary of given product and restaurant
     *
     * @param restaurantId
     */
    @Override
    public List<StockSummaryP> getProductSummary(Long restaurantId) {
        return stockSummaryRepository.getStockSummaries(restaurantId).orElseThrow(() -> new StockApiException.RestaurantNotFoundException(restaurantId.toString()));
    }

}
