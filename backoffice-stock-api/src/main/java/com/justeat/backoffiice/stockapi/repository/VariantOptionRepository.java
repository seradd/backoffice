package com.justeat.backoffiice.stockapi.repository;

import com.justeat.backoffiice.stockapi.domain.VariantOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VariantOptionRepository extends JpaRepository<VariantOption, Long> {


}
