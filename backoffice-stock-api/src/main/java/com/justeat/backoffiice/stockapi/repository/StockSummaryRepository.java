package com.justeat.backoffiice.stockapi.repository;

import com.justeat.backoffiice.stockapi.domain.StockSummary;
import com.justeat.backoffiice.stockapi.domain.projection.StockSummaryP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StockSummaryRepository extends JpaRepository<StockSummary, Long> {


    @Query(value = "SELECT M.*, R.NAME AS RESTAURANT,  P.NAME AS PRODUCT,  V.NAME AS VARIANT," +
            " VO.NAME AS VARIANTOPTION, SS.TOTAL_QUANTITY AS TOTALQUANTITY FROM " +
            " PRODUCT P, MENU M, PRODUCT_VARIANT_OPTION PVO, RESTAURANT R, " +
            " VARIANT V, VARIANT_OPTION VO, STOCK_SUMMARY SS" +
            " WHERE" +
            " R.ID = M.RESTAURANT_ID " +
            " AND PVO.ID = M.PRODUCT_VARIANT_OPTION_ID " +
            " AND P.ID = PVO. PRODUCT_ID " +
            " AND VO.ID = PVO. VARIANT_OPTION_ID" +
            " AND V.ID = VO.VARIANT_ID" +
            " AND SS.MENU_ID = M.ID" +
            " AND P.IS_ACTIVE = 1 AND M.IS_ACTIVE = 1" +
            " AND R.ID = :restaurantId"
            , nativeQuery = true)
    Optional<List<StockSummaryP>> getStockSummaries(@Param("restaurantId") Long restaurantId);


}
