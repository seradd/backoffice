package com.justeat.backoffiice.stockapi.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@Data
@NoArgsConstructor
@Entity
@Table(name = "PRODUCT_VARIANT_OPTION",uniqueConstraints={@UniqueConstraint(columnNames = {"productId", "variantOptionId"})})
public class ProductVariantOption extends BaseEntity {

    @JoinColumn(name = "PRODUCT_ID")
    private Long productId;

    @JoinColumn(name = "VARIANT_OPTION_ID")
    private Long variantOptionId;

}
