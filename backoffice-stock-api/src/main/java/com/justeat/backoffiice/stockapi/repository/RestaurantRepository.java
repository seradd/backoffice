package com.justeat.backoffiice.stockapi.repository;

import com.justeat.backoffiice.stockapi.domain.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {


}
