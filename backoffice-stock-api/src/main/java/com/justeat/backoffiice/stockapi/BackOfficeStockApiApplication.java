package com.justeat.backoffiice.stockapi;

import org.h2.tools.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class BackOfficeStockApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackOfficeStockApiApplication.class, args);
	}

	@Bean(initMethod = "start", destroyMethod = "stop")
	public Server inMemoryH2DatabaseServer() throws Exception {
		return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9021");
	}

}
