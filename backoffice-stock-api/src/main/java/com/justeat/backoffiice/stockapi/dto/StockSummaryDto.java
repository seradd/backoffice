package com.justeat.backoffiice.stockapi.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class StockSummaryDto {
    private String restaurant;
    private String product;
    private String variant;
    private String variantOption;
    private Integer totalQuantity;
}
