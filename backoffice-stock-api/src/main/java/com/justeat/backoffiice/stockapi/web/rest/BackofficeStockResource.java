package com.justeat.backoffiice.stockapi.web.rest;

import com.justeat.backoffiice.stockapi.dto.StockSummaryDto;
import com.justeat.backoffiice.stockapi.model.UpdateStockSummaryRequest;
import com.justeat.backoffiice.stockapi.service.BackofficeStockService;
import com.justeat.backoffiice.stockapi.web.rest.converter.StockSummaryConverter;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("v1/stock-api")
public class BackofficeStockResource {

    private final BackofficeStockService stockService;
    private final StockSummaryConverter stockSummaryConverter;

    @PutMapping("")
    @ApiOperation(value = "Updates Stock Summaries")
    public ResponseEntity<Void> updateStockSummary(@RequestBody UpdateStockSummaryRequest request) {
        log.debug("Stock is updating [{}]", request);
        stockService.updateStockSummary(request);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{restaurantId}")
    @ApiOperation(value = "Gets stock status of a restaurants products")
    public ResponseEntity<List<StockSummaryDto>> getStockSummary(@PathVariable Long restaurantId) {
        log.debug("Getting stock status restaurant:[{}]", restaurantId);
        return ResponseEntity.ok(stockSummaryConverter.toDtoList(stockService.getProductSummary(restaurantId)));
    }

}
