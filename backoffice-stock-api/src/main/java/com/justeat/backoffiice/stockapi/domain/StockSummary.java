package com.justeat.backoffiice.stockapi.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
@Table(name = "STOCK_SUMMARY")
public class StockSummary extends BaseEntity {

    @Column(name = "MENU_ID")
    private Long menuId;

    @Column(name = "TOTAL_QUANTITY")
    private Integer totalQuantity;

    private transient String restaurant;
    private transient String product;
    private transient String variant;
    private transient String variantOption;

}
