package com.justeat.backoffiice.stockapi.domain.projection;

public interface StockSummaryP {
    Long getId();
    String getRestaurant();
    String getProduct();
    String getVariant();
    String getVariantOption();
    Integer getTotalQuantity();
}
