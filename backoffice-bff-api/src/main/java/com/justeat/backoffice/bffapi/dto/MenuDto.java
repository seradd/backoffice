package com.justeat.backoffice.bffapi.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class MenuDto {

    private Long id;
    private String restaurant;
    private String category;
    private String product;
    private String variant;
    private String variantOption;
}
