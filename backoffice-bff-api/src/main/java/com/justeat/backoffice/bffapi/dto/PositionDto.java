package com.justeat.backoffice.bffapi.dto;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class PositionDto {
    private int x;
    private int y;
}
