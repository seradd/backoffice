package com.justeat.backoffice.bffapi.client;

import com.justeat.backoffice.bffapi.dto.StockSummaryDto;
import com.justeat.backoffice.bffapi.model.UpdateStockSummaryRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "backoffice-stock-api", url = "${backoffice-stock-api.ribbon.listOfServers}")
public interface BackofficeStockApiApiClient {

    @PutMapping("/stock-api")
    ResponseEntity<Void> updateStockSummary(@RequestBody UpdateStockSummaryRequest request);

    @GetMapping("/stock-api/{restaurantId}")
    ResponseEntity<List<StockSummaryDto>> getStockSummary(@PathVariable Long restaurantId);

}
