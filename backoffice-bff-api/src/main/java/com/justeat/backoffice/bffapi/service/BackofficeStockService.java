package com.justeat.backoffice.bffapi.service;

import com.justeat.backoffice.bffapi.dto.StockSummaryDto;
import com.justeat.backoffice.bffapi.model.UpdateStockSummaryRequest;

import java.util.List;

public interface BackofficeStockService {

    void updateStockSummary(UpdateStockSummaryRequest request);

    List<StockSummaryDto> getStockSummary(Long restaurantId);
}
