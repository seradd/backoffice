package com.justeat.backoffice.bffapi.service.impl;

import com.justeat.backoffice.bffapi.client.BackofficeStockApiApiClient;
import com.justeat.backoffice.bffapi.dto.StockSummaryDto;
import com.justeat.backoffice.bffapi.model.UpdateStockSummaryRequest;
import com.justeat.backoffice.bffapi.service.BackofficeStockService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class BackofficeStockServiceImpl implements BackofficeStockService {
    
    private final BackofficeStockApiApiClient backofficeStockApiApiClient;

    /**
     * Updates product stock with given specific restaurant and variant option
     *
     * @param request includes product, variant, restaurant and new quantity values
     */
    @Override
    public void updateStockSummary(UpdateStockSummaryRequest request) {
        backofficeStockApiApiClient.updateStockSummary(request);
    }

    /**
     * Gets product stock with given specific restaurant
     *
     * @param restaurantId id of the desired restaurant
     */
    @Override
    public List<StockSummaryDto> getStockSummary(Long restaurantId) {
        return backofficeStockApiApiClient.getStockSummary(restaurantId).getBody();
    }


}
