package com.justeat.backoffice.bffapi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UpdateStockSummaryRequest {

    private Long productId;
    private Long variantOptionId;
    private Long restaurantId;
    private Integer totalQuantity;
}
