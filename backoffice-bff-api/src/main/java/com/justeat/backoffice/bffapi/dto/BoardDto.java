package com.justeat.backoffice.bffapi.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class BoardDto {
    private List<PitDto> pits;
    private GameHistoryDto gameHistory;
}
