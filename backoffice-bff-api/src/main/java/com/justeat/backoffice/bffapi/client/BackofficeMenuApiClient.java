package com.justeat.backoffice.bffapi.client;

import com.justeat.backoffice.bffapi.dto.MenuDto;
import com.justeat.backoffice.bffapi.model.SyncMenuRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "backoffice-menu-api", url = "${backoffice-menu-api.ribbon.listOfServers}")
public interface BackofficeMenuApiClient {

    @PostMapping("/menu-api")
    ResponseEntity<Void> syncMenu(@Validated @RequestBody SyncMenuRequest gameHistoryDto);

    @GetMapping("/menu-api/{restaurantId}")
    ResponseEntity<List<MenuDto>> getMenuOfRestaurant(@Validated @PathVariable Long restaurantId);

}
