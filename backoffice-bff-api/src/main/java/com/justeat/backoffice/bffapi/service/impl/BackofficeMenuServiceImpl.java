package com.justeat.backoffice.bffapi.service.impl;

import com.justeat.backoffice.bffapi.client.BackofficeMenuApiClient;
import com.justeat.backoffice.bffapi.dto.MenuDto;
import com.justeat.backoffice.bffapi.model.SyncMenuRequest;
import com.justeat.backoffice.bffapi.service.BackofficeMenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class BackofficeMenuServiceImpl implements BackofficeMenuService {

    private final BackofficeMenuApiClient backofficeMenuApiClient;

    /**
     * Updates menu of a restaurant
     *
     * @param request
     */
    @Override
    public void syncMenu(SyncMenuRequest request) {
        backofficeMenuApiClient.syncMenu(request);
    }

    /**
     * Lists the menu of given restaurantId
     *
     * @param restaurantId id of the desired restaurant
     */
    @Override
    public List<MenuDto> getMenuOfRestaurant(Long restaurantId) {
        return backofficeMenuApiClient.getMenuOfRestaurant(restaurantId).getBody();
    }


}
