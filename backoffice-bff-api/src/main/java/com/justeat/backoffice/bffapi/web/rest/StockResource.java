package com.justeat.backoffice.bffapi.web.rest;

import com.justeat.backoffice.bffapi.dto.GameDto;
import com.justeat.backoffice.bffapi.dto.StockSummaryDto;
import com.justeat.backoffice.bffapi.model.UpdateStockSummaryRequest;
import com.justeat.backoffice.bffapi.service.BackofficeStockService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("v1/stock")
public class StockResource {

    private final BackofficeStockService backofficeStockService;

    @PutMapping("")
    @ApiOperation(value = "Updates Stock Summaries")
    public ResponseEntity<Void> updateStockSummary(@RequestBody UpdateStockSummaryRequest request) {
        log.debug("Stock is updating [{}]", request);
        backofficeStockService.updateStockSummary(request);
        return ResponseEntity.noContent().build();
    }

    @GetMapping ("/{restaurantId}")
    @ApiOperation(value = "Get stock Summary", response = GameDto.class)
    public ResponseEntity<List<StockSummaryDto>> newGame(@PathVariable Long restaurantId) {
        log.debug("Fetching stock Summary of :[{}]", restaurantId);
        return ResponseEntity.ok(backofficeStockService.getStockSummary(restaurantId));
    }

}
