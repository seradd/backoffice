package com.justeat.backoffice.bffapi.model;

import com.justeat.backoffice.bffapi.dto.GameDto;
import com.justeat.backoffice.bffapi.dto.PitDto;
import com.justeat.backoffice.bffapi.dto.PlayerDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class MoveRequest implements Serializable {
    private static final long serialVersionUID = 2515721895794079246L;

    public PitDto moveStartingPit;
    public PlayerDto currentPlayer;
    public GameDto game;
}
