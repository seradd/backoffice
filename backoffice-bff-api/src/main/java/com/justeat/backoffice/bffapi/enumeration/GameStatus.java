package com.justeat.backoffice.bffapi.enumeration;

public enum GameStatus {
    STARTED, SAVED, FINISHED
}
