package com.justeat.backoffice.bffapi.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PlayerDto {
    private String name;
    private PositionDto position;
    private boolean isCurrentPlayer;

}
