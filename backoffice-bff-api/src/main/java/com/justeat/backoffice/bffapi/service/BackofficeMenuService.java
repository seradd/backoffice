package com.justeat.backoffice.bffapi.service;

import com.justeat.backoffice.bffapi.dto.MenuDto;
import com.justeat.backoffice.bffapi.model.SyncMenuRequest;

import java.util.List;

public interface BackofficeMenuService {

    void syncMenu(SyncMenuRequest request);

    List<MenuDto> getMenuOfRestaurant(Long restaurantId);
}
