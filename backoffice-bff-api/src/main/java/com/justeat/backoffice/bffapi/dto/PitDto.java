package com.justeat.backoffice.bffapi.dto;

import com.justeat.backoffice.bffapi.enumeration.PitType;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class PitDto {
    private PitType pitType;
    private PositionDto position;
    private PositionDto oppositePitPosition;
    private int numberOfStones;
}