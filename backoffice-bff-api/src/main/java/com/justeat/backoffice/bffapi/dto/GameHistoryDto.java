package com.justeat.backoffice.bffapi.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class GameHistoryDto {
   private String player1;
   private String player2;
   private LocalDate createdDateTime;
   private String winner;
}
