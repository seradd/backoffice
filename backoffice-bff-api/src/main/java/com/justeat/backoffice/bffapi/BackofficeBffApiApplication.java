package com.justeat.backoffice.bffapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class BackofficeBffApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackofficeBffApiApplication.class, args);
	}

}
