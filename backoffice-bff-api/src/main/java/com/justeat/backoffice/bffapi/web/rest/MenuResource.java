package com.justeat.backoffice.bffapi.web.rest;

import com.justeat.backoffice.bffapi.model.SyncMenuRequest;
import com.justeat.backoffice.bffapi.service.BackofficeMenuService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("v1/menu")
public class MenuResource {

    private final BackofficeMenuService backofficeMenuService;


    @PutMapping("")
    @ApiOperation(value = "Sync Menu Summaries")
    public ResponseEntity<Void> syncMenu(@RequestBody SyncMenuRequest request) {
        log.debug("Menu is updating [{}]", request);
        backofficeMenuService.syncMenu(request);
        return ResponseEntity.noContent().build();
    }

    @GetMapping ("/{restaurantId}")
    @ApiOperation(value = "Sync Menu Summaries")
    public ResponseEntity<Void> getMenu(@PathVariable Long restaurantId) {
        log.debug("Menu is fething [{}]", restaurantId);
        backofficeMenuService.getMenuOfRestaurant(restaurantId);
        return ResponseEntity.noContent().build();
    }


}
