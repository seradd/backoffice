package com.justeat.backoffice.bffapi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NewGameRequest {
    private String player1;
    private String player2;
}
