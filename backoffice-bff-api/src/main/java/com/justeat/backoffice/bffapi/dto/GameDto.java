package com.justeat.backoffice.bffapi.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class GameDto {
    private List<PlayerDto> players;
    private BoardDto board;
    private boolean haveOldGame;
    private boolean isOver;
    private List<GameHistoryDto> gameHistories;

    @Override
    public String toString() {
        return "GameDto{" +
                "players=" + players +
                ", board=" + board +
                ", haveOldGame=" + haveOldGame +
                ", isOver=" + isOver +
                '}';
    }
}
