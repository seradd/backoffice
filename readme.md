<p align="center">



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#introduction">Introduction</a></li>
    <li><a href="#used-tech">Used Tech</a></li>
    <li><a href="#arch">Arch</a></li>
    <li><a href="#database">Database</a></li>
    <li><a href="#howto">Build and Running Instructions</a></li>
    <li><a href="#Build and Running Instructions">Conclusion</a></li>
  </ol>
</details>



<!-- INTRODUCTION -->
## Introduction
A simple implementation of backoffice application that can handle stock statuses of products and menu of restaurants.
</br>


--------------
## Used Tech

1. Java 11
2. H2db
3. Spring Boot 2.6.2
4. Lombok
5. Mapstruct
6. Swagger (http://localhost:9000/swagger-ui/index.html#/ ports: 9020, 9040)

--------------
<!-- Arch -->
## Arch
There are 3 micro-services;
- backoffice-bff-api is back-end-for-frontend which behaves like gateway, only endpoint for clients and which aggregates all the requests then returns a single response.
- backoffice-stock-api is responsible for stock transactions and stock status of a product
- backoffice-menu-api stores the menu of a restaurant on H2db.

</br></br>

Hi-Level
![alt text](design.png)
</br></br>

DB Design
![alt text](db.png)
</br></br>

--------------
<!-- Database -->
## Database

* `******** Game History Datase Info *********`
- `url:` http://localhost:9020/h2-console
- `-h2 console login parameters-`
- `Driver Class:`org.h2.Driver
- `JDBC URL:`jdbc:h2:mem:backofficedb
- `User Name:`sa
- `password:` 1

<!-- Build and Running Instructions -->
## Build and Running Instructions
be-> backoffice-bff-api, backoffice-stock-api, backoffice-menu-api
(note: start with stock-api to make up db)
- mvn spring-boot:run
- bff-api http://localhost:9000/swagger-ui/index.html#/
- engine-api http://localhost:9020/swagger-ui/index.html#/
- history-api http://localhost:9040/swagger-ui/index.html#/

with docker:
go to ../backoffice-bff-api directory

1)mvn clean package
2)docker build --tag=backoffice-bff-api:v1.1 .
3)docker run -p9000:9000 backoffice-bff-api:v1.1

go to ../backoffice-stock-api directory
1)mvn clean package
2)docker build --tag=backoffice-stock-api:v1.1 .
3)docker run -p9020:9020 backoffice-stock-api:v1.1

go to ../backoffice-menu-api directory
1)mvn clean package
2)docker build --tag=backoffice-menu-api:v1.1 .
3)docker run -p9040:9040 backoffice-menu-api:v1.1

--------------
<!-- Conclusion -->
## conclusion
This project offers a solution for handling stock statuses and menu updates for restaurants. 
It includes 3 micro-services h2 db(common usage for all but embeded in stock-api) java 11.