package com.justeat.backoffice.stockapi.service;


import com.justeat.backoffiice.menuapi.exception.MenuApiException;
import com.justeat.backoffiice.menuapi.repository.MenuRepository;
import com.justeat.backoffiice.menuapi.service.BackofficeMenuServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BackofficeMenuServiceTest {

    @Mock
    private MenuRepository repository;
    @InjectMocks
    private BackofficeMenuServiceImpl backofficeMenuService;

    @Test
    void whenRestaurantNotFound_ShouldThrowRestaurantNotFoundException() {
        when(repository.getRestaurantMenu(1L)).thenReturn(Optional.empty());
        assertThrows(MenuApiException.MenuNotFoundException.class, () -> backofficeMenuService.getMenuOfRestaurant(1L));
    }

}
