package com.justeat.backoffiice.menuapi.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
@Table(name = "RESTAURANT")
public class Restaurant extends BaseEntity {

    @Column(name = "NAME")
    private String restaurantName;

    @Column(name = "ADDRESS")
    private String address;
}
