package com.justeat.backoffiice.menuapi.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@Data
@NoArgsConstructor
@Entity
@Table(name = "MENU",uniqueConstraints={@UniqueConstraint(columnNames = {"restaurantId", "productVariantOptionId"})})
public class Menu extends BaseEntity {

    @JoinColumn(name = "RESTAURANT_ID")
    private Long restaurantId;

    @JoinColumn(name = "PRODUCT_VARIANT_OPTION_ID")
    private Long productVariantOptionId;

}

@Data
class ImportMenuRequest {
    private Long restaurantId;
    private Long productVariantOptionId;
}