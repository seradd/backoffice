package com.justeat.backoffiice.menuapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class MenuApiException {

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class MenuNotFoundException extends RuntimeException{
        public MenuNotFoundException(String message) {
            super("Menu Not Found: " +message);
        }
    }
}
