package com.justeat.backoffiice.menuapi.domain.enumeration;

public enum OperationTypeEnum {
    IN, OUT
}
