package com.justeat.backoffiice.menuapi.domain;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "VARIANT")
public class Variant extends BaseEntity {

    @Column(name = "NAME")
    private String variantName;

    @OneToMany
    private Set<VariantOption> variantOptions;

    @Override
    public String toString() {
        return "Variant{" +
                "id=" + id +
                ", variantName='" + variantName + '\'' +
                ", variantOptions=" + variantOptions +
                '}';
    }
}
