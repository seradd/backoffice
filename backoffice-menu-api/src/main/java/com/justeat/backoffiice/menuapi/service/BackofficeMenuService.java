package com.justeat.backoffiice.menuapi.service;

import com.justeat.backoffiice.menuapi.domain.projection.MenuP;
import com.justeat.backoffiice.menuapi.model.SyncMenuRequest;

import java.util.List;

public interface BackofficeMenuService {

    void syncMenu(SyncMenuRequest request);

    List<MenuP> getMenuOfRestaurant(Long restaurantId);

}
