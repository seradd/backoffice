package com.justeat.backoffiice.menuapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
@Table(name = "CATEGORY")
public class Category extends BaseEntity {

    @Column(name = "NAME")
    private String categoryName;

    @Column(name = "PARENT_ID")
    private Long parentId;

    @ManyToMany(fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Product> products;

}
