package com.justeat.backoffiice.menuapi.domain;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "VARIANT_OPTION")
public class VariantOption extends BaseEntity {

    @Column(name = "NAME")
    private String variantOptionName;

    @ManyToOne
    @JoinColumn(name="VARIANT_ID")
    private Variant variant;


    @Override
    public String toString() {
        return "VariantOption{" +
                "id=" + id +
                ", variantOptionName='" + variantOptionName + '\'' +
                ", variant=" + variant +
                '}';
    }
}
