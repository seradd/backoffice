package com.justeat.backoffiice.menuapi.web.rest;

import com.justeat.backoffiice.menuapi.dto.MenuDto;
import com.justeat.backoffiice.menuapi.model.SyncMenuRequest;
import com.justeat.backoffiice.menuapi.service.BackofficeMenuService;
import com.justeat.backoffiice.menuapi.web.rest.converter.MenuConverter;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("v1/menu-api")
public class BackofficeMenuResource {

    private final BackofficeMenuService menuService;
    private final MenuConverter menuConverter;

    @PutMapping("")
    @ApiOperation(value = "Sync Menu Of Restaurant")
    public ResponseEntity<Void> syncMenu(@RequestBody SyncMenuRequest request) {
        log.debug("Menu is updating [{}]", request);
        menuService.syncMenu(request);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{restaurantId}")
    @ApiOperation(value = "Gets menu of a restaurant")
    public ResponseEntity<List<MenuDto>> getMenuOfRestaurant(@PathVariable Long restaurantId) {
        log.debug("Getting menu of restaurant:[{}]", restaurantId);
        return ResponseEntity.ok(menuConverter.toDtoList(menuService.getMenuOfRestaurant(restaurantId)));
    }

}
