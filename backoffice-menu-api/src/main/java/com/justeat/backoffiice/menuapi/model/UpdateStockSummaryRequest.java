package com.justeat.backoffiice.menuapi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UpdateStockSummaryRequest {

    private Long menuId;
    private Long productVariantOptionId;
    private Integer totalQuantity;

}
