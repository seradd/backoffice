package com.justeat.backoffiice.menuapi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SyncMenuRequest {

    private Long restaurantId;
    private Long productVariantOptionId;


}
