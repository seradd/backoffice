package com.justeat.backoffiice.menuapi.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@MappedSuperclass
@Data
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(
            name = "base_seq_generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = SequenceStyleGenerator.OPT_PARAM, value = "pooled-lo"),
                    @Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1000"),
                    @Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1"),
                    @Parameter(name = SequenceStyleGenerator.CONFIG_PREFER_SEQUENCE_PER_ENTITY, value = "true"),
                    @Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "BASE_SEQ"),
            }
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "base_seq_generator")
    @Basic(optional = false)
    @Column(name = "id", unique = true, nullable = false, precision = 15, scale = 0, columnDefinition = "NUMERIC")
    protected Long id;

    @UpdateTimestamp
    @Column(name = "LAST_UPDATED", columnDefinition = "TIMESTAMP")
    private LocalDateTime lastUpdatedDateTime;

    @CreationTimestamp
    @Column(name = "CREATED_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime createdDateTime;

    @Column(name = "IS_ACTIVE", nullable = false)
    private Boolean isActive = true;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BaseEntity baseEntity = (BaseEntity) o;
        if (baseEntity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), baseEntity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString()
    {
        return "BaseEntity{" + "id=" + getId() + "}";
    }
}
