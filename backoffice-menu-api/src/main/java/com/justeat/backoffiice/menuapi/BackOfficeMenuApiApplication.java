package com.justeat.backoffiice.menuapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackOfficeMenuApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackOfficeMenuApiApplication.class, args);
	}

}
