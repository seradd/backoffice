package com.justeat.backoffiice.menuapi.service;

import com.justeat.backoffiice.menuapi.domain.Menu;
import com.justeat.backoffiice.menuapi.domain.projection.MenuP;
import com.justeat.backoffiice.menuapi.exception.MenuApiException;
import com.justeat.backoffiice.menuapi.model.SyncMenuRequest;
import com.justeat.backoffiice.menuapi.repository.MenuRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class BackofficeMenuServiceImpl implements BackofficeMenuService {

    private final MenuRepository menuRepository;

    /**
     * Synchronizes the menu category,variant etc.
     *
     * @param request includes menuId, restaurantId and new quantity values
     */
    @Override
    public void syncMenu(SyncMenuRequest request) {
        final var menu = new Menu();
        menu.setRestaurantId(request.getRestaurantId());
        menu.setProductVariantOptionId(request.getProductVariantOptionId());
        menuRepository.save(menu);
    }

    /**
     * Lists the menu of given restaurantId
     *
     * @param restaurantId
     */
    @Override
    public List<MenuP> getMenuOfRestaurant(Long restaurantId) {
        return menuRepository.getRestaurantMenu(restaurantId).orElseThrow(() -> new MenuApiException.MenuNotFoundException(restaurantId.toString()));
    }

}
