package com.justeat.backoffiice.menuapi.web.rest.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.justeat.backoffiice.menuapi.domain.projection.MenuP;
import com.justeat.backoffiice.menuapi.dto.MenuDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MenuConverter {
    ObjectMapper INSTANCE = Mappers.getMapper( ObjectMapper.class );

    @Mapping(source = "id", target = "id")
    List<MenuDto> toDtoList(List<MenuP> menus);

}
